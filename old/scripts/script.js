// Some Custom Garbage
jQuery(function($) {

    var mb = {

        batclass: function() {

            var height = $('.batarang-content.revealed').outerHeight();
            if (height > 0) {
                $('.batarang-content-wrapper').css('min-height', height + 'px');
            }

        },

        batcycle: function() {
            $(function() {
                FastClick.attach(document.body);
            });
        },

        bathook: function() {
            $('.batslider').flexslider({
                animation: "fade",
                slideshow: "true",
                slideshowSpeed: "6000",
                animationSpeed: "500",
                pauseOnHover: true,
                touch: true,
                directionNav: true,
                controlNav: false,
                animationLoop: true,
                prevText: "",
                nextText: "",
                keyboard: true,
                multipleKeyboard: true
            });
        }

    }

    // Fire Them Up
    mb.batclass(); // height on portfolio content wrapper
    mb.batcycle(); // fastclick
    mb.bathook(); // flexslider

    // Window Resize
    $(window).on('load resize', function() {
        mb.batclass();
    });

});
