/*global module:false*/
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        // JSON
        pkg: grunt.file.readJSON('package.json'),
        // Generate Meta
        meta: {
            version: '3.3.1',
        },
        // JS concat
        concat: {
            dist: {
                src: [
                    '<banner:meta.banner>',
                    '../js/vendor/jquery.js',
                    '../js/vendor/jquery.flexslider.js',
                    '../js/vendor/ga.js',
                    '../js/partials/*.js'
                ],
                dest: '../js/pre/nina.js'
            }
        },
        // CSS Min
        cssmin: {
            options: {
                banner: '/*! nina berlingeri v3 - v<%= meta.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
                    '* http://www.ninaberlingeri.com/\n' +
                    '* Copyright (c) <%= grunt.template.today("yyyy") %> ' +
                    'nina; Licensed MIT */' +
                    '/*! \n' +
                    'Theme Name: nina v3 \n' +
                    'Theme URI: http://www.ninaberlingeri.com \n' +
                    'Description: Version 3.2 of Lexichronic \n' +
                    'Author: Marko Bajlovic \n' +
                    'Author URI: http://www.marko.tech \n' +
                    'Version: 2.0.1 \n' +
                    '*/'
            },
            dist: {
                src: ['../css/style.css'],
                dest: '../style.css'
            }
        },
        // Watch
        watch: {
            configFiles: {
                files: ['Gruntfile.js']
            },
            css: {
                files: ['<config:lint.files>', '../sass/**/*.scss', '../sass/**/**/*.scss'],
                tasks: ['default']
            },
            scripts: {
                files: ['<config:lint.files>', '../js/partials/*.js', '../js/vendor/*.js'],
                tasks: ['default']
            }
        },
        // Compass
        compass: {
            dist: {
                forcecompile: true
            }
        },
        // Uglify
        uglify: {
            my_target: {
                files: {
                    '../js/nina.min.js': ['../js/pre/nina.js']
                }
            }
        },
        // Notify
        notify_hooks: {
            options: {
                enabled: true,
                max_jshint_notifications: 5,
                title: "Grunt: Rose Renegades Compiled",
                success: true,
                duration: 4
            }
        }
    });

    // Default task
    grunt.registerTask('default', ['concat', 'cssmin', 'compass', 'uglify']);

    // CSS tasks
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    // Watch
    grunt.loadNpmTasks('grunt-contrib-watch');
    // Concat
    grunt.loadNpmTasks('grunt-contrib-concat');
    // Compass
    grunt.loadNpmTasks('grunt-contrib-compass');
    // Uglify
    grunt.loadNpmTasks('grunt-contrib-uglify');
    // Notify
    grunt.loadNpmTasks('grunt-notify');

    // Run Tasks
    grunt.task.run('notify_hooks');

};
