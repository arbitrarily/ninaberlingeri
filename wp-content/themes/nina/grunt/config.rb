# Theme directories:
http_path = ""
css_dir = "../css"
sass_dir = "../sass"
images_dir = "../img"
javascripts_dir = "../js"

# You can select your preferred output style here (can be overridden via the command line):
output_style = :compact
environment = :production

# To enable relative paths to assets via compass helper functions.
relative_assets = true

# To disable debugging comments that display the original location of your selectors.
# line_comments = false

#FireSass
sass_options = {:debug_info => false}
