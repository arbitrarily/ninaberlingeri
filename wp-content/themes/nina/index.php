<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nina
 */

get_header(); ?>

	<div id="primary" class="content-area row">
		<main id="main" class="site-main" role="main">
			<div class="flexslider columns small-12 medium-11 large-10 medium-centered">
				<ul class="slides row">
					<?php
					if (have_posts()) :
						while (have_posts()) : the_post();
							echo '<li>';
							get_template_part('template-parts/content', get_post_format());
							echo '</li>';
						endwhile;
					endif;
					?>
				</ul>
			</div>
		</main>
	</div>

	<div id="secondary" class="row">
		<div class="archive-by-year columns small-12 medium-10 medium-centered">
			<ul><?php wp_get_archives('type=yearly'); ?></ul>
		</div>
	</div>

<?php
get_footer();
