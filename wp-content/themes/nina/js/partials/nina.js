jQuery(function($) {

	var nina = {

		init: function() {

			$(window).on('load resize', function() {
				nina.slider();
			});

		},

		slider: function() {
            $('.flexslider').flexslider({
                animation: 'slide',
                easing: 'swing',
                pauseOnHover: true,
                useCSS: true,
                touch: true,
				itemWidth: 815,
				itemMargin: 0,
				minItems: 1,
				maxItems: 2
            });
		}

	}

	nina.init();

});
