<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nina
 */

?>

<article id="post-<?php the_ID(); ?>" class="columns small-12">
	<div class="featured-image">
		<?php the_post_thumbnail(); ?>
	</div>
	<header class="entry-header">
		<?php the_title( '<h4 class="entry-title">', '</h4>' ); ?>
	</header>
	<div class="entry-content">
		<?php truncateByWord(the_content(), 120, ".."); ?>
	</div>
</article>
